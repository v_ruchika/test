<?php
include_once('connection.php');
?>
<!DOCTYPE html>
<html>
<title>Test Project</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karma">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Karma", sans-serif}
.w3-bar-block .w3-bar-item {padding:20px}
</style>
<body>

<!-- Sidebar (hidden by default) -->
<nav class="w3-sidebar w3-bar-block w3-card w3-top w3-xlarge w3-animate-left" style="display:none;z-index:2;width:40%;min-width:300px" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()"
  class="w3-bar-item w3-button">Close Menu</a>
  <a href="#addcat" onclick="w3_close()" class="w3-bar-item w3-button">Add Category</a>
  <a href="#products" onclick="w3_close()" class="w3-bar-item w3-button">Add Product</a>
</nav>

<!-- Top menu -->
<div class="w3-top">
  <div class="w3-white w3-xlarge" style="max-width:1200px;margin:auto">
    <div class="w3-button w3-padding-16 w3-left" onclick="w3_open()">☰</div>
    <div class="w3-right w3-padding-16"></div>
    <div class="w3-center w3-padding-16">Test WEB</div>
  </div>
</div>

<!------------------Add Castegory Start------------------------------->
<div class="w3-main w3-content w3-padding" style="max-width:1200px;margin-top:200px; margin-bottom:400px;">

  <!-- First Photo Grid-->
      <div class="w3-row-padding w3-padding-16 w3-center" id="addcat">
            <?php
            // define variables and set to empty values
            $nameErr  ="";
            $name = "";

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $check1 = 0;
              if (empty($_POST["name"])) {
              $nameErr = " Category Name is required";
              } else {
                  $name = test_input($_POST["name"]);
                  // check if name only contains letters and whitespace
                  if (!preg_match("/^[a-z0-9A-Z-' ]*$/",$name)) {
                  $nameErr = "Only letters and white space and number allowed";
                  }else{
                    $check1++;
                  }
              }
              if (empty($_POST["sname"])) {
              $snameErr = " Category Name is required";
              } else {
                  $sname = test_input($_POST["sname"]);
                  // check if name only contains letters and whitespace
                  if (!preg_match("/^[a-z0-9A-Z-' ]*$/",$sname)) {
                  $snameErr = "Only letters and white space and number allowed";
                  }else{
                    $check1++;
                  }
              }
            }


            $sql = "SELECT id FROM category";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {

            $id = $row["id"];
            $id++;
            }
            } else {
            echo "0 results";
            }  



            if($check1 >0){
              $sql = "INSERT INTO category (id, name)
              VALUES ('".$id."','".$name."');";
              if ($conn->multi_query($sql) === TRUE) {
              echo "Category Added successfully";
              } else {
              echo "Error: " . $sql . "<br>" . $conn->error;
              }
              $sql = "INSERT INTO sub_category (name,cid)
              VALUES ('".$name."','".$id."');";
              if ($conn->multi_query($sql) === TRUE) {
              echo "Sub Category added successfully";
              } else {
              echo "Error: " . $sql . "<br>" . $conn->error;
              }
            }else{}
            ?>
      </div>
      <div class="w3-container w3-padding-32 w3-center">  
          <h3>Add Category</h3><br>
          <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
          Category  Name: <input type="text" name="name" value="<?php echo $name;?>">
          <span class="error">* <?php echo $nameErr;?></span>
          <br><br>
           Sub Category  Name: <input type="text" name="sname" value="<?php echo $sname;?>">
          <span class="error">* <?php echo $snameErr;?></span>
          <br><br>
          <input type="submit" name="submit" value="Submit">  
          </form>
      </div>  
</div>
<!------------------Add Castegory END------------------------------->

<!------------------Add Product start------------------------------->
<div class="w3-main w3-content w3-padding" style="max-width:1200px;margin-top:200px; margin-bottom:400px;">

  <!-- First Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center" id="products">
    <?php
    // define variables and set to empty values
    $nameErr  ="";
    $name = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $check =0;
        if (empty($_POST["pname"])) {
          $pnameErr = "Please Enter Product Name";
           $check =0;
        } else {
          $pname = test_input($_POST["pname"]);
          // check if name only contains letters and whitespace
          if (!preg_match("/^[a-z0-9A-Z-' ]*$/",$name)) {
             $check =0;
            $pnameErr = "Only letters and white space and numberallowed";
          }else{
            $check++;
          }
        }


        if (empty($_POST["sku"])) {
          $skuErr = "Please Enter SKU";
           $check =0;
        } else {
          $sku = test_input($_POST["sku"]);
          // check if name only contains letters and whitespace
          if (!is_numeric($sku)) {
            $skuErr = "Please Enter Number Only";
             $check =0;
          }else{
            $check++;
          }
        }


        if (empty($_POST["price"])) {
           $check =0;
          $priceErr = "Please Enter price";
        } else {
          $price = test_input($_POST["price"]);
          // check if name only contains letters and whitespace
          if (!is_numeric($price)) {
             $check =0;
            $priceErr = "Please Enter Number Only";
          }else{
            $check++;
          }
        }
    }
    function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
    }
    if($check > 0){
      $sql = "INSERT INTO product (name, sku, price)
      VALUES ('".$name."','".$sku."','".$sku."')";
      if ($conn->multi_query($sql) === TRUE) {
      echo "Product Is added Successfully";
      } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }
    ?>
  </div>
  <!-- Second Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center">
  <h3>Add Product</h3><br>
  <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Product  Name: <input type="text" name="pname" value="<?php echo $pname;?>">
  <span class="error">* <?php echo $pnameErr;?></span>
  <br><br>
  Product SKU <input type="text" name="sku" value="<?php echo $skuError;?>">
  <span class="error">* <?php echo $skuErr;?></span>
  <br><br>
  Product price <input type="text" name="price" value="<?php echo $price;?>">
  <span class="error">* <?php echo $priceErr;?></span>
  <br><br>
  Product Category: 
  <select name="cars" id="cars">
      <option value="volvo">Please Select Category</option>
        <?php
        $sql = "SELECT name FROM category";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
        // output data of each row
          while($row = $result->fetch_assoc()) {
            if(!empty($row["name"])){?>

            <option value="c">
            <?php
                   echo $row["name"];
            ?>
            </option>
            <?php
            }
          }
        } else {
          echo "0 results";
        }   
        ?>
  </select><br><br>
  <input type="submit" name="submit" value="Submit">  
  </form>
  </div>  
 
</div>
<!------------------Add Product END------------------------------->

<script type="text/javascript" src="\test_project/asset/js/custom.js"></script>

</body>
</html>
